<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TutorialController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get(TutorialController::URL_API_GET_TUTORIAL_BY_ID, [TutorialController::class, TutorialController::METHOD_GET_TUTORIAL_BY_ID]);
Route::post(TutorialController::URL_API_CREATE_TUTORIAL, [TutorialController::class, TutorialController::METHOD_CREATE_TUTORIAL]);
Route::get(TutorialController::URL_API_GET_LIST_TUTORIAL, [TutorialController::class, TutorialController::METHOD_GET_LIST_TUTORIAL]);
Route::put(TutorialController::URL_API_UPDATE_TUTORIAL, [TutorialController::class, TutorialController::METHOD_UPDATE_TUTORIAL]);
Route::delete(TutorialController::URL_API_DELETE_TUTORIAL, [TutorialController::class, TutorialController::METHOD_DELETE_TUTORIAL]);
