<?php

namespace App\Http\Controllers;

use App\Models\Tutorial;
use Illuminate\Http\Request;

class TutorialController extends Controller
{
    const URL_API_GET_TUTORIAL_BY_ID = '/tutorial/get-by-id/{id}';
    const URL_API_GET_LIST_TUTORIAL = '/tutorial/get-list';
    const URL_API_CREATE_TUTORIAL = '/tutorial/create-tutorial';
    const URL_API_UPDATE_TUTORIAL = '/tutorial/update-tutorial/{id}';
    const URL_API_DELETE_TUTORIAL = '/tutorial/delete-tutorial/{tutorialId}';

    const METHOD_GET_TUTORIAL_BY_ID = 'getById';
    const METHOD_GET_LIST_TUTORIAL = 'getList';
    const METHOD_CREATE_TUTORIAL = 'createTutorial';
    const METHOD_UPDATE_TUTORIAL = 'updateTutorial';
    const METHOD_DELETE_TUTORIAL = 'deleteTutorial';

    const KEY_STATUS_CODE = 'statusCode';
    const KEY_DATA = 'data';
    const KEY_MESSAGE = 'message';

    /**
     * Display a tutorial get by id.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getById($id ,Request $request)
    {
        $tutorial = Tutorial::find($id);
        $response = [
            self::KEY_STATUS_CODE => 200,
            self::KEY_DATA => $tutorial,
            self::KEY_MESSAGE => 'Success'
        ];
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createTutorial(Request $request)
    {
        $dataCreate = [
            Tutorial::COL_TITLE => $request->title,
            Tutorial::COL_DESCRIPTION => $request->description
        ];
        $tutorial = Tutorial::create($dataCreate);
        $response = [
            self::KEY_STATUS_CODE => 200,
            self::KEY_DATA => $tutorial,
            self::KEY_MESSAGE => 'Success'
        ];
        return response()->json($response, 200);
    }

    /**
     * Show list tutorial
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        $tutorials = Tutorial::all()->toArray();
        $response = [
            self::KEY_STATUS_CODE => 200,
            self::KEY_DATA => $tutorials,
            self::KEY_MESSAGE => 'Success'
        ];
        return response()->json($response, 200);
    }

    /**
     * Update a tutorial
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateTutorial($id, Request $request)
    {
        $tutorial = Tutorial::find($id);
        $tutorial->{Tutorial::COL_TITLE} = $request->title;
        $tutorial->{Tutorial::COL_DESCRIPTION} = $request->description;
        $tutorial->save();
        $response = [
            self::KEY_STATUS_CODE => 200,
            self::KEY_DATA => $tutorial,
            self::KEY_MESSAGE => 'Success'
        ];
        return response()->json($response, 200);
    }

    /**
     * Delete a tutorial
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteTutorial($tutorialId)
    {
        $tutorial = Tutorial::find($tutorialId);
        $tutorial->delete();
        $response = [
            self::KEY_STATUS_CODE => 200,
            self::KEY_DATA => [],
            self::KEY_MESSAGE => 'Delete Success'
        ];
        return response()->json($response, 200);
    }
}
