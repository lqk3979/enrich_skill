<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutorial extends Model
{
    use SoftDeletes;

    const COL_ID = 'id';
    const COL_TITLE = 'title';
    const COL_DESCRIPTION = 'description';

    use HasFactory;

    protected $fillable = [
        self::COL_TITLE,
        self::COL_DESCRIPTION
    ];
}
